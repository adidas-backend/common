
package com.adidas.common.dto.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "standard_price",
    "standard_price_no_vat",
    "currentPrice"
})
@Data
public class PricingInformation {

    @JsonProperty("standard_price")
    public long standardPrice;
    @JsonProperty("standard_price_no_vat")
    public float standardPriceNoVat;
    @JsonProperty("currentPrice")
    public long currentPrice;

}
