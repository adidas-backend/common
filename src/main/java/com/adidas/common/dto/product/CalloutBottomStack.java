
package com.adidas.common.dto.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "iconID",
    "title",
    "body",
    "link_text"
})
@Data
public class CalloutBottomStack {

    @JsonProperty("id")
    public String id;
    @JsonProperty("iconID")
    public String iconID;
    @JsonProperty("title")
    public String title;
    @JsonProperty("body")
    public String body;
    @JsonProperty("link_text")
    public String linkText;

}
