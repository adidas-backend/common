package com.adidas.common.dto.product;

import com.adidas.common.dto.review.ReviewResponseDTO;

import lombok.Data;

@Data
public class ProductResponseDTO {
	
	private ReviewResponseDTO reviewDetails;
	
	private Product productDetails;

}
