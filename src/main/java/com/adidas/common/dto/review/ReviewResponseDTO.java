package com.adidas.common.dto.review;

import java.util.List;

import lombok.Data;

@Data
public class ReviewResponseDTO {

	List<ReviewScoreDTO> reviews;
}
