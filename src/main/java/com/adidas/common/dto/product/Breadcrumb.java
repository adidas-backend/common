
package com.adidas.common.dto.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "text",
    "link"
})
@Data
public class Breadcrumb {

    @JsonProperty("text")
    public String text;
    @JsonProperty("link")
    public String link;

}
