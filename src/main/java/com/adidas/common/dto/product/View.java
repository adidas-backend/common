
package com.adidas.common.dto.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "image_url",
    "source",
    "metadata"
})
@Data
public class View {

    @JsonProperty("type")
    public String type;
    @JsonProperty("image_url")
    public String imageUrl;
    @JsonProperty("source")
    public String source;
    @JsonProperty("metadata")
    public Metadata metadata;

}
