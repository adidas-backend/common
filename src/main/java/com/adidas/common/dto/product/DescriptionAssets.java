
package com.adidas.common.dto.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "image_url",
    "video_url",
    "poster_url"
})
@Data
public class DescriptionAssets {

    @JsonProperty("image_url")
    public String imageUrl;
    @JsonProperty("video_url")
    public Object videoUrl;
    @JsonProperty("poster_url")
    public Object posterUrl;

}
