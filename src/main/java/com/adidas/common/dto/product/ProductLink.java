
package com.adidas.common.dto.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "name",
    "url",
    "pricing_information",
    "default_color",
    "search_color",
    "productId",
    "image",
    "source",
    "altImage",
    "badge_text",
    "badge_style"
})
@Data
public class ProductLink {

    @JsonProperty("type")
    public String type;
    @JsonProperty("name")
    public String name;
    @JsonProperty("url")
    public String url;
    @JsonProperty("pricing_information")
    public PricingInformation pricingInformation;
    @JsonProperty("default_color")
    public String defaultColor;
    @JsonProperty("search_color")
    public String searchColor;
    @JsonProperty("productId")
    public String productId;
    @JsonProperty("image")
    public String image;
    @JsonProperty("source")
    public String source;
    @JsonProperty("altImage")
    public String altImage;
    @JsonProperty("badge_text")
    public String badgeText;
    @JsonProperty("badge_style")
    public String badgeStyle;

}
