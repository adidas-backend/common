
package com.adidas.common.dto.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "value",
    "selectedMarkerIndex",
    "markerCount"
})
@Data
public class SizeFitBar {

    @JsonProperty("value")
    public String value;
    @JsonProperty("selectedMarkerIndex")
    public long selectedMarkerIndex;
    @JsonProperty("markerCount")
    public long markerCount;

}
