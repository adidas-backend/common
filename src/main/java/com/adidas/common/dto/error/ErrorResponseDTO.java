package com.adidas.common.dto.error;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponseDTO {

	private ErrorCodes status;
	private String error;
	private long timeStampMillis;
	Map<String,Object> errorMap;
}
