
package com.adidas.common.dto.product;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "care_instructions"
})
@Data
public class WashCareInstructions {

    @JsonProperty("care_instructions")
    public List<Object> careInstructions = null;

}
