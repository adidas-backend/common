
package com.adidas.common.dto.product;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "title",
    "subtitle",
    "text",
    "description_assets",
    "usps",
    "wash_care_instructions"
})
@Data
public class ProductDescription {

    @JsonProperty("title")
    public String title;
    @JsonProperty("subtitle")
    public String subtitle;
    @JsonProperty("text")
    public String text;
    @JsonProperty("description_assets")
    public DescriptionAssets descriptionAssets;
    @JsonProperty("usps")
    public List<String> usps = null;
    @JsonProperty("wash_care_instructions")
    public WashCareInstructions washCareInstructions;

}
