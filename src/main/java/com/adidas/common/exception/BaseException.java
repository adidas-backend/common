package com.adidas.common.exception;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

import com.adidas.common.dto.error.ErrorCodes;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BaseException extends RuntimeException implements Serializable {
	
	
	private static final long serialVersionUID = -5226470090962069210L;
	private String message;
	private ErrorCodes code;
    private HttpStatus httpStatus;

}
