package com.adidas.common.dto.review;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import lombok.Data;

@Data
public class ReviewScoreDTO {

	@Min(value=1)
	@Max(value=5)
	private int rating;
	private String productId;
	private Long userId;
}
