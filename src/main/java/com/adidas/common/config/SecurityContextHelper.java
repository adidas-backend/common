package com.adidas.common.config;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class SecurityContextHelper {

	private SecurityContextHelper() {
        // reduced visibility to avoid abuse
    }
	
	public static UserDetails getUser() {
		if(SecurityContextHolder.getContext().getAuthentication()!=null && SecurityContextHolder.getContext().getAuthentication() instanceof UsernamePasswordAuthenticationToken  )
			return (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		else return null;
	}
	
	public static String getUserId() {
		UserDetails userDetails = getUser();
		return userDetails!=null?userDetails.getUsername():null;
	}
}
