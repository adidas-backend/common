
package com.adidas.common.dto.product;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "collection",
    "productType",
    "sport",
    "sportSub",
    "isInPreview",
    "customizable",
    "isCnCRestricted",
    "isWaitingRoomProduct",
    "mandatory_personalization",
    "outlet",
    "personalizable",
    "sale",
    "specialLaunch",
    "special_launch_type",
    "gender",
    "category",
    "size_fit_bar",
    "search_color",
    "search_color_raw",
    "brand",
    "color",
    "mtbr_flag",
    "pricebook",
    "coming_soon_signup",
    "preview_to",
    "size_chart_link"
})
@Data
public class AttributeList {

    @JsonProperty("collection")
    public List<String> collection = null;
    @JsonProperty("productType")
    public List<String> productType = null;
    @JsonProperty("sport")
    public List<String> sport = null;
    @JsonProperty("sportSub")
    public List<String> sportSub = null;
    @JsonProperty("isInPreview")
    public boolean isInPreview;
    @JsonProperty("customizable")
    public boolean customizable;
    @JsonProperty("isCnCRestricted")
    public boolean isCnCRestricted;
    @JsonProperty("isWaitingRoomProduct")
    public boolean isWaitingRoomProduct;
    @JsonProperty("mandatory_personalization")
    public boolean mandatoryPersonalization;
    @JsonProperty("outlet")
    public boolean outlet;
    @JsonProperty("personalizable")
    public boolean personalizable;
    @JsonProperty("sale")
    public boolean sale;
    @JsonProperty("specialLaunch")
    public boolean specialLaunch;
    @JsonProperty("special_launch_type")
    public String specialLaunchType;
    @JsonProperty("gender")
    public String gender;
    @JsonProperty("category")
    public String category;
    @JsonProperty("size_fit_bar")
    public SizeFitBar sizeFitBar;
    @JsonProperty("search_color")
    public String searchColor;
    @JsonProperty("search_color_raw")
    public String searchColorRaw;
    @JsonProperty("brand")
    public String brand;
    @JsonProperty("color")
    public String color;
    @JsonProperty("mtbr_flag")
    public boolean mtbrFlag;
    @JsonProperty("pricebook")
    public String pricebook;
    @JsonProperty("coming_soon_signup")
    public boolean comingSoonSignup;
    @JsonProperty("preview_to")
    public String previewTo;
    @JsonProperty("size_chart_link")
    public String sizeChartLink;

}
