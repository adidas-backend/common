
package com.adidas.common.dto.product;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "model_number",
    "name",
    "meta_data",
    "product_type",
    "product_description",
    "recommendationsEnabled",
    "pricing_information",
    "attribute_list",
    "product_link_list",
    "view_list",
    "breadcrumb_list",
    "callouts",
    "embellishment"
})
@Data
public class Product {

    @JsonProperty("id")
    public String id;
    @JsonProperty("model_number")
    public String modelNumber;
    @JsonProperty("name")
    public String name;
    @JsonProperty("meta_data")
    public Metadata metaData;
    @JsonProperty("product_type")
    public String productType;
    @JsonProperty("product_description")
    public ProductDescription productDescription;
    @JsonProperty("recommendationsEnabled")
    public boolean recommendationsEnabled;
    @JsonProperty("pricing_information")
    public PricingInformation pricingInformation;
    @JsonProperty("attribute_list")
    public AttributeList attributeList;
    @JsonProperty("product_link_list")
    public List<ProductLink> productLinkList = null;
    @JsonProperty("view_list")
    public List<View> viewList = null;
    @JsonProperty("breadcrumb_list")
    public List<Breadcrumb> breadcrumbList = null;
    @JsonProperty("callouts")
    public Callouts callouts;
    @JsonProperty("embellishment")
    public Embellishment embellishment;

}
