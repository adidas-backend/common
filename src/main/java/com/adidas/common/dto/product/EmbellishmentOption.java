
package com.adidas.common.dto.product;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "position",
    "positionPrice",
    "allowChooseOwnText",
    "fields"
})
@Data
public class EmbellishmentOption {

    @JsonProperty("position")
    public String position;
    @JsonProperty("positionPrice")
    public long positionPrice;
    @JsonProperty("allowChooseOwnText")
    public boolean allowChooseOwnText;
    @JsonProperty("fields")
    public List<Field> fields = null;

}
