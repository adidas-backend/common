
package com.adidas.common.dto.product;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "imageStyle",
    "view",
    "usageTerms",
    "sortOrder",
    "subjects"
})
@Data
public class Metadata {

    @JsonProperty("imageStyle")
    public String imageStyle;
    @JsonProperty("view")
    public String view;
    @JsonProperty("usageTerms")
    public String usageTerms;
    @JsonProperty("sortOrder")
    public String sortOrder;
    @JsonProperty("subjects")
    public List<Object> subjects = null;

}
