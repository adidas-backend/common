
package com.adidas.common.dto.product;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "callout_bottom_stack"
})
@Data
public class Callouts {

    @JsonProperty("callout_bottom_stack")
    public List<CalloutBottomStack> calloutBottomStack = null;

}
