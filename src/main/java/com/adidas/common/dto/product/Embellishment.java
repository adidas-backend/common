
package com.adidas.common.dto.product;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "embellishmentOptions",
    "articleType"
})
@Data
public class Embellishment {

    @JsonProperty("embellishmentOptions")
    public List<EmbellishmentOption> embellishmentOptions = null;
    @JsonProperty("articleType")
    public String articleType;

}
