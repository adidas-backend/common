
package com.adidas.common.dto.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "key",
    "maxLength",
    "minLength",
    "validation",
    "textColor",
    "usesStock",
    "stockCollection"
})
@Data
public class Field {

    @JsonProperty("type")
    public String type;
    @JsonProperty("key")
    public String key;
    @JsonProperty("maxLength")
    public long maxLength;
    @JsonProperty("minLength")
    public long minLength;
    @JsonProperty("validation")
    public String validation;
    @JsonProperty("textColor")
    public String textColor;
    @JsonProperty("usesStock")
    public boolean usesStock;
    @JsonProperty("stockCollection")
    public Object stockCollection;

}
